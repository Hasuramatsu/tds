// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSInventoryComponent.h"

#include "TPS/Game/TPSGameInstance.h"

// Sets default values for this component's properties
UTPSInventoryComponent::UTPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	for(int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
		if(myGI)
		{
			if(!WeaponSlots[i].ItemName.IsNone())
			{
				FWeaponInfo Info;
				if(myGI->GetWeaponInfoByName(WeaponSlots[i].ItemName, Info))
				{
					WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
				}
				else
				{
					WeaponSlots.RemoveAt(i);
					i--;
				}
			}
		}
	}

	MaxSlotsWeapon = WeaponSlots.Num();

	if(WeaponSlots.IsValidIndex(0))
	{
		if(!WeaponSlots[0].ItemName.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].ItemName, WeaponSlots[0].AdditionalInfo);
		}
	}
}


// Called every frame
void UTPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if(ChangeToIndex > WeaponSlots.Num()-1)
	{
		CorrectIndex = 0;
	}
	else
	{
		if(ChangeToIndex < 0)
		{
			CorrectIndex = WeaponSlots.Num() - 1;
		}
	}
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;

	int8 i = 0;
	while(i < WeaponSlots.Num() && !bIsSuccess)
	{
		if(i == CorrectIndex)
		{
			if(!WeaponSlots[i].ItemName.IsNone())
			{
				NewIdWeapon = WeaponSlots[i].ItemName;
				NewAdditionalInfo = WeaponSlots[i].AdditionalInfo;
				bIsSuccess = true;
			}			
		}
		i++;
	}
	if(!bIsSuccess)
	{
		
	}
	if(bIsSuccess)
	{
		SetWeaponAdditionalInfo(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UTPSInventoryComponent::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	FAdditionalWeaponInfo tmp;
	return tmp;
}

int32 UTPSInventoryComponent::GetWeaponIndexByName(FName IdWeaponName)
{
	bool bIsSuccess = false;
	for(int i = 0; i < WeaponSlots.Num(); i++)
	{
		if(WeaponSlots[i].ItemName == IdWeaponName)
		{
			return i;
		}
	}
	return 0;
}

void UTPSInventoryComponent::SetWeaponAdditionalInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if(WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if(i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;
			}
			i++;
		}
		if(!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetWeaponAdditionalInfo - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetWeaponAdditionalInfo - Not correct weapon index - %d"), IndexWeapon);
	}
}

void UTPSInventoryComponent::ChangeCurrentAmmo(int32 AmmoCount, EWeaponType WeaponType)
{
	AmmoSlots[WeaponType].CurrentAmmo += AmmoCount;
	if(AmmoSlots[WeaponType].CurrentAmmo > AmmoSlots[WeaponType].MaxAmmo)
	{
		AmmoSlots[WeaponType].CurrentAmmo = AmmoSlots[WeaponType].MaxAmmo;
	}
	else if(AmmoSlots[WeaponType].CurrentAmmo < 0)
	{
		AmmoSlots[WeaponType].CurrentAmmo = 0;
	}
	OnChangeAmmo.Broadcast();
}

int8 UTPSInventoryComponent::GetLoadAmmoCount(EWeaponType WeaponType, int8 AmmoUsed)
{
	int8 Load = 0;
	if(AmmoSlots[WeaponType].CurrentAmmo >= AmmoUsed)
	{
		Load = AmmoUsed;
	}
	else
	{
		Load = AmmoSlots[WeaponType].CurrentAmmo;
	}
	ChangeCurrentAmmo(-AmmoUsed, WeaponType);
	return Load;
}

bool UTPSInventoryComponent::IsAmmoFull(EWeaponType WeaponType)
{
	return AmmoSlots[WeaponType].CurrentAmmo == AmmoSlots[WeaponType].MaxAmmo;
}

bool UTPSInventoryComponent::IsWielded(FName IdWeaponName)
{
	bool check = false;
	if(GetWeaponIndexByName(IdWeaponName))
	{
		check = true;
	}
	return check;	
}

void UTPSInventoryComponent::PickupWeapon(AWeaponPickup &Weapon)
{
	if(IsWielded(Weapon.Title))
	{
		if(!IsAmmoFull(Weapon.WeaponType))
		{
			ChangeCurrentAmmo(Weapon.CurrentRound, Weapon.WeaponType);
			Weapon.Destroy();
		}
	}
	else if(!Weapon.Title.IsNone())
	{
		bool HaveSlots = false;
		for(int i = 0; i < WeaponSlots.Num(); i++)
		{
			if(WeaponSlots[i].ItemName.IsNone())
			{
				HaveSlots = true;
				WeaponSlots[i].ItemName = Weapon.Title;
				WeaponSlots[i].AdditionalInfo.Round = Weapon.CurrentRound;
				Weapon.Destroy();
				OnSwitchWeapon.Broadcast(WeaponSlots[i].ItemName, WeaponSlots[i].AdditionalInfo);
				break;
			}
		}
		if(!HaveSlots)
		{
			OnWeaponNeedSwap.Broadcast(&Weapon);
		}
	}
}




