// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Items/Pickup/WeaponPickup.h"


#include "TPSInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSwitchWeapon, FName, WeaponIDName, FAdditionalWeaponInfo,
                                             WeaponAdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNeedSwap, AWeaponPickup*, Weapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeAmmo);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UTPSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTPSInventoryComponent();

	UPROPERTY(BlueprintAssignable)
	FOnSwitchWeapon OnSwitchWeapon;
	UPROPERTY()
	FOnWeaponNeedSwap OnWeaponNeedSwap;
	UPROPERTY(BlueprintAssignable)
	FOnChangeAmmo OnChangeAmmo;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons") 
	TMap<EWeaponType, FAmmoSlot> AmmoSlots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	int32 MaxSlotsWeapon = 0;

	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo);

	FAdditionalWeaponInfo GetAdditionalWeaponInfo(int32 IndexWeapon);
	int32 GetWeaponIndexByName(FName IdWeaponName);
	void SetWeaponAdditionalInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);
	void ChangeCurrentAmmo(int32 AmmoUsed, EWeaponType WeaponType);

	int8 GetLoadAmmoCount(EWeaponType WeaponType, int8 AmmoUsed);
	bool IsAmmoFull(EWeaponType WeaponType);

	bool IsWielded(FName IdWeaponName);

	void PickupWeapon(AWeaponPickup &Weapon);

	
};
