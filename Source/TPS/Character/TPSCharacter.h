// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "TPSInventoryComponent.h"
#include "GameFramework/Character.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Items/PickUp.h"
#include "TPS/Items/Weapon/WeaponDefault.h"
#include "TPSCharacter.generated.h"

DECLARE_DELEGATE_OneParam(FChangeWeaponByBool, const bool);
DECLARE_DELEGATE_OneParam(FChangeWeaponByInt, const int);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPickupOverlap, APickUp*, Pickup);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPickupOverlapEnd, APickUp*, Pickup);

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	ATPSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Component)
	UTPSInventoryComponent* InventoryComponent = nullptr;
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.f,40.f,40.f);

	class UDecalComponent* CurrentCursor = nullptr;
	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Statement")
	bool bIsAiming = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Statement")
	bool bIsSprinting = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Statement")
	bool bIsCrouching = false;

	float AxisX = 0.f;
	float AxisY = 0.f;
	
	//Camera 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Slide")
	float MaxCameraHeight = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Slide")
	float MinCameraHeight = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Slide")
	float CameraSlideStep = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Slide")
	float CameraSladeDistance = 250;


	float MouseWheelAxis = 0.f;
	float TargetHeight;
	bool bIsCameraOut = false;
	FTimerHandle MemberTimerHandle;
	bool bNeedTarget = false;

	//Weapon
		//for test
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TEST")
		FName InitWeaponName;

	UPROPERTY()
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY()
	AActor* CurrentInteractTarget = nullptr;
	

	//UPROPERTY()
	//TMap<EWeaponType, FAmmoSlot>* AmmoSlot = nullptr;

	//Delegate func
	UPROPERTY(BlueprintAssignable)
	FOnPickupOverlap OnPickupOverlap;
	UPROPERTY(BlueprintAssignable)
	FOnPickupOverlapEnd OnPickupOverlapEnd;
	
	//Input func
	UFUNCTION()
    void InputAxisX(float Value);
	UFUNCTION()
    void InputAxisY(float Value);
	UFUNCTION()
    void InputMouseWheelAxis(float Value);
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();
	UFUNCTION()
	void ChangeWeapon(bool isNext);
	UFUNCTION()
	void ChangeWeaponByInt(int Index);


	//TickFunc
	UFUNCTION()
    void MovementTick(float DeltaTime);

	//Movement update func
	UFUNCTION(BlueprintCallable)
    void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
    void ChangeMovementState(EMovementState NewMovementState);

	//Camera update func
	UFUNCTION()
	void CameraHeightChange(float DeltaTime);
	void CameraZoomIn();
	void CameraZoomOut();

	//WeaponFunc
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName WeaponID, FAdditionalWeaponInfo AdditionalWeaponInfo);
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
    void WeaponFireStart_BP(UAnimMontage* Anim);

	//WeaponReload func
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoUsed);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	//Cursor func
	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	//test
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentWeaponIndex = 0;

	//Pickup Func
	UFUNCTION()
	void OverlapPickup(APickUp *Pickup);
	UFUNCTION()
	void OverlapPickupEnd(APickUp *Pickup);
	UFUNCTION()
	void SetInteractableTarget(AWeaponPickup *Weapon);

	//Drop
	void DropCurrentWeapon();

	//InteractFunc
	UFUNCTION()
	void Interact();
};












