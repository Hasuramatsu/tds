// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSCharacter.h"

#include <string>


#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TPS/Game/TPSGameInstance.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("Inventory Component"));

	if(InventoryComponent)
	{
		InventoryComponent->OnWeaponNeedSwap.AddDynamic(this, &ATPSCharacter::SetInteractableTarget);
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPSCharacter::InitWeapon);
	}
	
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if(CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if(myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
	CameraHeightChange(DeltaSeconds);
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	
	
	if(CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);
	
	NewInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATPSCharacter::InputMouseWheelAxis);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::TryReloadWeapon);
	NewInputComponent->BindAction<FChangeWeaponByBool>(TEXT("NextWeapon"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeapon, true);
	NewInputComponent->BindAction<FChangeWeaponByBool>(TEXT("PrevWeapon"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeapon, false);
	NewInputComponent->BindAction<FChangeWeaponByInt>(TEXT("FirstSlot"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeaponByInt, 0);
	NewInputComponent->BindAction<FChangeWeaponByInt>(TEXT("SecondSlot"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeaponByInt, 1);
	NewInputComponent->BindAction<FChangeWeaponByInt>(TEXT("ThirdSlot"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeaponByInt, 2);
	NewInputComponent->BindAction<FChangeWeaponByInt>(TEXT("FourthSlot"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeaponByInt, 3);
	NewInputComponent->BindAction<FChangeWeaponByInt>(TEXT("FifthSlot"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeaponByInt, 4);
	NewInputComponent->BindAction<FChangeWeaponByInt>(TEXT("SixthSlot"), EInputEvent::IE_Released, this, &ATPSCharacter::ChangeWeaponByInt, 5);
	NewInputComponent->BindAction(TEXT("DropWeapon"), EInputEvent::IE_Released, this, &ATPSCharacter::DropCurrentWeapon);
	NewInputComponent->BindAction(TEXT("Interact"), EInputEvent::IE_Released, this, &ATPSCharacter::Interact);

}

void ATPSCharacter::InputMouseWheelAxis(float Value)
{
	MouseWheelAxis = Value;
}

void ATPSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::ChangeWeapon(bool isNext)
{
	int8 coef = 1;
	if(!isNext)
	{
		coef = -1;
	}
	if(InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentWeaponIndex;
		FAdditionalWeaponInfo OldInfo;
		if(CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if(CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
			if(InventoryComponent->SwitchWeaponToIndex(CurrentWeaponIndex + coef, OldIndex, OldInfo))
			{				
			}
		}
	}
}

void ATPSCharacter::ChangeWeaponByInt(int Index)
{
	if(Index < InventoryComponent->WeaponSlots.Num())
	{
		int8 OldIndex = CurrentWeaponIndex;
		if(Index != OldIndex)
		{
			FAdditionalWeaponInfo OldInfo;
			if(CurrentWeapon)
			{
				OldInfo = CurrentWeapon->WeaponInfo;
				if(CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
				if(InventoryComponent->SwitchWeaponToIndex(Index, OldIndex, OldInfo))
				{
				
				}
			}
		}
	}
}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPSCharacter::MovementTick(float DeltaTime)
{
	if(MovementState != EMovementState::Sprint_State)
	{
		AddMovementInput(FVector(1.f,0.f,0.f), AxisX);
		AddMovementInput(FVector(0.f,1.f,0.f), AxisY);
	}
	else
	{
			AddMovementInput(GetActorForwardVector(), 1);
	}

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if(MyController)
	{
		FHitResult ResultHit;
		MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true,ResultHit);
		float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f,Yaw,0.0f)));

		if(CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			if(bIsAiming)
			{
				Displacement = FVector(0.f,0.f, 160.f);
			}
			else
			{
				Displacement = FVector(0.f, 0.f, 120.f);
			}
		//	UE_LOG(LogTemp, Warning, TEXT("ShootLocation : %s"), *(ResultHit.Location).ToString());
			CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
		}
	}
	if(CurrentWeapon)
	{
		if(FMath::IsNearlyZero(GetVelocity().Size(),0.5f))
		{
			CurrentWeapon->bIsIdle = true;
		}
		else
		{
			CurrentWeapon->bIsIdle = false;
		}
	}
}

void ATPSCharacter::CharacterUpdate()
{
	float ResultSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::Run_State:
		ResultSpeed = MovementInfo.RunSpeed;
		bIsCrouching = false;
		bIsSprinting = false;
		if(CurrentWeapon)
		{
			CurrentWeapon->ShouldReduceDispersion = true;
		}
		break;
	case EMovementState::Crouch_State:
		ResultSpeed = MovementInfo.CrouchSpeed;
		bIsSprinting = false;
		if(CurrentWeapon)
		{
			CurrentWeapon->ShouldReduceDispersion = true;
		}
		break;
	case EMovementState::Sprint_State:
		ResultSpeed = MovementInfo.SprintSpeed;
		bIsCrouching = false;
		bIsAiming = false;
		if(CurrentWeapon)
		{
			CurrentWeapon->ShouldReduceDispersion = false;
		}
		break;
	}
	if(bIsAiming && !bIsSprinting)
	{
		ResultSpeed *= MovementInfo.AimSpeedMulti;
	}

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState, bIsAiming);
	}
	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATPSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	
	MovementState = NewMovementState;
	CharacterUpdate();
}

void ATPSCharacter::CameraHeightChange(float DeltaTime)
{
 ;
	if(!bIsCameraOut)
	{
		if(MouseWheelAxis > 0)
		{
			TargetHeight = CameraBoom->TargetArmLength - CameraSladeDistance;
			if(TargetHeight <= MinCameraHeight)
			{
				TargetHeight = MinCameraHeight;
			}
			bIsCameraOut = true;
			GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ATPSCharacter::CameraZoomIn, 0.02, true);
		}
		else if(MouseWheelAxis < 0)
		{
			TargetHeight = CameraBoom->TargetArmLength + CameraSladeDistance;
			if(TargetHeight >= MaxCameraHeight)
			{
				TargetHeight = MaxCameraHeight;
			}
			bIsCameraOut = true;
			GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ATPSCharacter::CameraZoomOut, 0.02, true);
		}
	}
	

}

void ATPSCharacter::CameraZoomIn()
{
	if(CameraBoom->TargetArmLength > TargetHeight)
	{
		CameraBoom->TargetArmLength -= CameraSlideStep;
	}
	
	if(CameraBoom->TargetArmLength <= TargetHeight)
	{
		GetWorldTimerManager().ClearTimer(MemberTimerHandle);
		bIsCameraOut = false;
	}
}

void ATPSCharacter::CameraZoomOut()
{
	if(CameraBoom->TargetArmLength < TargetHeight)
	{
		CameraBoom->TargetArmLength += CameraSlideStep;
	}
	if(CameraBoom->TargetArmLength >= TargetHeight)
	{
		GetWorldTimerManager().ClearTimer(MemberTimerHandle);
		bIsCameraOut = false;
	}
}

void ATPSCharacter::InitWeapon(FName WeaponID, FAdditionalWeaponInfo AdditionalWeaponInfo)
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if(myGI)
	{
		if(myGI->GetWeaponInfoByName(WeaponID, myWeaponInfo))
		{
			if(myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if(myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(),Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->InitProjectile(myWeaponInfo.ProjectileID);
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->UpdateStateWeapon(MovementState, bIsAiming);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATPSCharacter::WeaponFireStart);

					myWeapon->WeaponInfo = AdditionalWeaponInfo;
					if(InventoryComponent)
					{
						CurrentWeaponIndex = InventoryComponent->GetWeaponIndexByName(WeaponID);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - WeaponInfo -NULL"))
		}
	}
}

void ATPSCharacter::TryReloadWeapon()
{
	if(CurrentWeapon)
	{
		if(CurrentWeapon->WeaponInfo.Round < CurrentWeapon->WeaponSetting.MaxRound && !(CurrentWeapon->WeaponReloading))
		{
			if(InventoryComponent)
			{
				if(InventoryComponent->AmmoSlots[CurrentWeapon->WeaponSetting.WeaponType].CurrentAmmo > 0)
				{
					CurrentWeapon->InitReload();
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("Not enough ammo!"));
				}
				int i = 0;
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("No inventory"));
			}
		}
	}
	else
	{
		UE_LOG(LogTemp,Warning,TEXT("No weapon"));
	}
}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacte::AttackCharEvet - Current Weapon -NULL"));
	}
}

void ATPSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	WeaponFireStart_BP(Anim);
}

void ATPSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATPSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoUsed)
{
	if(bIsSuccess)
	{
		if(InventoryComponent && CurrentWeapon)
		{
			if(InventoryComponent->WeaponSlots[CurrentWeaponIndex].InfinityAmmo)
			{
				CurrentWeapon->WeaponInfo.Round = AmmoUsed;
			}
			else
			{
				CurrentWeapon->WeaponInfo.Round = InventoryComponent->GetLoadAmmoCount(CurrentWeapon->WeaponSetting.WeaponType, AmmoUsed);
			}		
		}
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATPSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
}

UDecalComponent* ATPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATPSCharacter::OverlapPickup(APickUp *Pickup)
{
	AWeaponPickup* WeaponPickup = nullptr;
	OnPickupOverlap.Broadcast(Pickup);
	switch ((*Pickup).PickupType)
	{
	case EPickupType::WeaponType:
		WeaponPickup = Cast<AWeaponPickup>(Pickup);
	if(WeaponPickup)
	{
		InventoryComponent->PickupWeapon((*WeaponPickup));
	}
		break;
	case EPickupType::AmmoType:
		(*Pickup).Interact(this);
		break;
	case EPickupType::MedkitType:
		
		break;
	default:
		UE_LOG(LogTemp, Warning, TEXT("No matching pickup type"));
		break;
	}
}

void ATPSCharacter::OverlapPickupEnd(APickUp* Pickup)
{
	OnPickupOverlapEnd.Broadcast(Pickup);
	AActor* OverlappedActor = Cast<AActor>(Pickup);
	if(OverlappedActor)
	{
		if(OverlappedActor == CurrentInteractTarget)
		{
			CurrentInteractTarget = nullptr;
		}
	}
}

void ATPSCharacter::SetInteractableTarget(AWeaponPickup* Weapon)
{
	CurrentInteractTarget = Weapon;
}


void ATPSCharacter::DropCurrentWeapon()
{
	if(InventoryComponent->WeaponSlots[CurrentWeaponIndex].CanBeDropped)
	{
		if(InventoryComponent)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			FVector Location = GetActorLocation() + GetActorForwardVector() * 200;
			FRotator Rotation = GetActorRotation();
			AWeaponPickup* DroppedWeapon = Cast<AWeaponPickup>(GetWorld()->SpawnActor(AWeaponPickup::StaticClass(), &Location, &Rotation, SpawnParams));
			UE_LOG(LogTemp, Warning, TEXT("%s"), *DroppedWeapon->GetName());
			if(DroppedWeapon)
			{
				DroppedWeapon->InitParams(InventoryComponent->WeaponSlots[CurrentWeaponIndex].ItemName, CurrentWeapon->WeaponInfo.Round);
				InventoryComponent->WeaponSlots[CurrentWeaponIndex].ItemName = "None";
				InventoryComponent->WeaponSlots[CurrentWeaponIndex].AdditionalInfo.Round = 0;
				//TODO maybe another logic
				// AActor* DropWeaponPtr = Cast<AActor>(DroppedWeapon);
				// if(DropWeaponPtr)
				// {
				// 	CurrentInteractTarget = DropWeaponPtr;
				// }
				
				ChangeWeaponByInt(0); //TODO change weapon on drop
			}
		}
	}
}

void ATPSCharacter::Interact()
{
	if(CurrentInteractTarget)
	{
		IInteractable* Interface = Cast<IInteractable>(CurrentInteractTarget);
		if(Interface)
		{
			Interface->Interact(this);
		}
	}
}
