// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ObjectMacros.h"

#include "Types.generated.h"


UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Run_State UMETA(DisplayName = "Run State"),
    Crouch_State UMETA(DisplayName = "Crouch State"),
    Sprint_State UMETA(DisplayName = "Sprint State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
    RifleType UMETA(DisplayName = "Rifle"),
    SniperRifleType UMETA(DisplayName = "Sniper Rifle"),
    ShotgunType UMETA(DisplayName = "Shotgun"),
    PistolType UMETA(DisplayName = "Pistol"),
    GrenadeLauncherType UMETA(DisplayName = "Grenade Launcher"),
    RocketLauncherType UMETA(DisplayName = "Rocket Launcher")
};

UENUM(BlueprintType)
enum class EPickupType : uint8
{
    WeaponType UMETA(DisplayName = "Weapon"),
    AmmoType UMETA(DisplayName = "Ammo"),
    MedkitType UMETA(DisplayName = "Medkit"),
    None UMETA(DisplayName = "None")
};


USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeed = 600.f;   
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float CrouchSpeed = 250.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float SprintSpeed = 800.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeedMulti = 0.3f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    TSubclassOf<class AProjectileDefault> Projectile = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    UStaticMesh* ProjectileMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    UParticleSystem* ProjectileTrail = nullptr;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    float ProjectileDamage = 20.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    float ProjectileLifeTime = 20.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    float ProjectileInitSpeed = 2000.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFX;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    USoundBase* SurfaceHitSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
    USoundBase* BodyHitSound = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive")
    UParticleSystem* ExploseFX = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive")
    USoundBase* ExplosiveSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive")
    float ExploseMaxDamage = 200.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive")
    float ExploseMinDamage = 200.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive")
    float DamageInnerRadius = 100.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive")
    float DamageOuterRadius = 200.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive")
    float DamageFalloff = 5.f;
       
};

USTRUCT(BlueprintType)
struct  FWeaponDispersion
{
    GENERATED_BODY()
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionStart = 0.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionMax = 1.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionMin = 0.3f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionRecoil = 0.1f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionReduction = 0.1f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionAimCoef = 0.75f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionCrouchCoef = 0.9f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionIdleMin = 0.1f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float DispersionIdleMax = 0.4f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
    TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
    EWeaponType WeaponType;
     
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    float RateOfFire = 0.05f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    float ReloadTime = 2.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    int32 MaxRound = 10;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    int32 ProjectilesByShot = 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    FWeaponDispersion DispersionWeapon;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundBase* SoundFireWeapon = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundBase* SoundReloadWeapon = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    UParticleSystem* EffectFireWeapon = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    FName ProjectileID;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
    float WeaponDamage = 20.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
    float DistanceTrace = 2000.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Animation")
    UAnimSequence* AnimWeaponReload = nullptr;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
    UDecalComponent* DecalOnHit = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
    UAnimMontage* AnimCharFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
    UAnimMontage* AnimCharReload = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    UStaticMesh* MagazineDrop = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    UParticleSystem* SleeveBullets = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    float SwitchTimeToWeapon = 1.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    UTexture2D* WeaponIcon = nullptr;
};



USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
    int32 Round = 10;
    
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slot")
    FName ItemName;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slot")
    FAdditionalWeaponInfo AdditionalInfo;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slot")
    bool CanBeDropped = true;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slot")
    bool InfinityAmmo = false;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo Slot")
    int32 CurrentAmmo = 100;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo Slot")
    int32 MaxAmmo = 100;
};

USTRUCT(BlueprintType)
struct FDropInfo : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Info")
    UStaticMesh* DropStaticMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Info")
    USkeletalMesh* DropSkeletalMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    UParticleSystem* DropFX = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    USoundBase* DropSound = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Info")
    FName Title;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Info")
    EPickupType PickupType = EPickupType::None;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
    EWeaponType WeaponType = EWeaponType::PistolType;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
    int32 CurrentRound = 0;
};

UCLASS()
class TPS_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
};

