// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"


#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Grenade::BeginPlay()
{
    Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
    if(TimerEnabled)
    {
        if(TimerToExplose >= TimeToExplose)
        {
            Explose();
        }
        else
        {
            TimerToExplose += DeltaTime;
        }
    }
    
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
                                                         UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
    ImpactProjectile();
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
    TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
    TimerEnabled = false;
    if(ProjectileSettings.ExploseFX)
    {
        UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.f));
    }
    if(ProjectileSettings.ExplosiveSound)
    {
        UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExplosiveSound, GetActorLocation());
    }
    TArray<AActor*> IgnoreActor;
    UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
        ProjectileSettings.ExploseMaxDamage,
        ProjectileSettings.ExploseMinDamage,
        GetActorLocation(),
        ProjectileSettings.DamageInnerRadius,
        ProjectileSettings.DamageOuterRadius,
        ProjectileSettings.DamageFalloff, NULL, IgnoreActor, nullptr, nullptr);
    if(ShowDebug)
    {
        DrawDebugSphere(GetWorld(), GetActorLocation(),ProjectileSettings.DamageInnerRadius, 20, FColor::Red, false, 10);
        DrawDebugSphere(GetWorld(), GetActorLocation(),ProjectileSettings.DamageOuterRadius, 20, FColor::Green, false, 10);
    }
    this->Destroy();
}
