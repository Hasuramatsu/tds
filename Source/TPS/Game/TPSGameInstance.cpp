// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSGameInstance.h"

bool UTPSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
    bool bIsFind = false;
    FWeaponInfo* WeaponInfoRow;
    if(WeaponInfoTable)
    {
        WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
        if(WeaponInfoRow)
        {
            bIsFind = true;
            OutInfo = *WeaponInfoRow;
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("No loaded weapon table."))
    }
    return bIsFind;
}

bool UTPSGameInstance::GetProjectileInfoByName(FName NameProjectile, FProjectileInfo& OutInfo)
{
    bool bIsFind = false;
    FProjectileInfo* ProjectileInfoRow;
    if(ProjectileInfoTable)
    {
        ProjectileInfoRow = ProjectileInfoTable->FindRow<FProjectileInfo>(NameProjectile, "", false);
        if(ProjectileInfoRow)
        {
            bIsFind = true;
            OutInfo = *ProjectileInfoRow;
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("No loaded projectile table."))
    }
    return bIsFind;
}

bool UTPSGameInstance::GetDropInfoByName(FName DropName, FDropInfo& OutInfo)
{
    bool bIsFind = false;
    FDropInfo* DropInfoRow;
    if(DropInfoTable)
    {
      DropInfoRow = DropInfoTable->FindRow<FDropInfo>(DropName, "", false);
        if(DropInfoRow)
        {
            bIsFind = true;
            OutInfo = *DropInfoRow;
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("No loaded drop info table."))
    }
    return bIsFind;
}


