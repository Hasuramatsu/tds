// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TPS/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TPS/Items/Weapon/WeaponDefault.h"
#include "TPSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UTPSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	UDataTable* ProjectileInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropInfo")
	UDataTable* DropInfoTable = nullptr;
	
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon,  FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
    bool GetProjectileInfoByName(FName NameProjectile,  FProjectileInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
    bool GetDropInfoByName(FName NameProjectile,  FDropInfo& OutInfo);
};
