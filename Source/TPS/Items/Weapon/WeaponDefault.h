// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TPS/FuncLibrary/Types.h"
#include "TPS/Items/Weapon/Projectiles/ProjectileDefault.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoUsed);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireEnd);
 
UCLASS()
class TPS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	UPROPERTY(BlueprintAssignable)
	FOnWeaponReloadStart OnWeaponReloadStart;
	UPROPERTY(BlueprintAssignable)
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	UPROPERTY(BlueprintAssignable)
	FOnWeaponFireStart OnWeaponFireStart;
	UPROPERTY(BlueprintAssignable)
	FOnWeaponFireEnd OnWeaponFireEnd;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;
	
	UPROPERTY()
	FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo WeaponInfo;
	UPROPERTY()
	FProjectileInfo ProjectileSettings;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);


	//Initialization
	void WeaponInit();
	void InitReload();
	void InitProjectile(FName ProjectileID);
	bool bIsProjInit = false;

	//Fire
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	float FireTimer = -0.1f;
	
	bool CheckWeaponCanFire();
	void Fire();
	UFUNCTION(BlueprintNativeEvent)
	void Fire_BP();
	
	//Reload
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reload")
	bool WeaponReloading = false;
	UPROPERTY(BlueprintReadOnly)
	float ReloadTimer = -0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reload")
	float ReloadTime = 0.0f;
	
	
	void FinishReload();
	void CancelReload();
	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire);
	void SpawnMagazine();

	//Getters
	UFUNCTION(BlueprintCallable)
    int32 GetWeaponRound();
	UFUNCTION(BlueprintCallable)
    FVector GetFireEndLocation() const;
	//TODO remove
	UFUNCTION(BlueprintCallable)
    float GetReloadTime();//
	FName GetProjectileID();
	float GetCurrentDispersion() const;
	int8 GetNumberProjectileByShot() const;
	UFUNCTION(BlueprintCallable)
	UAnimSequence* GetReloadAnimation();
	UFUNCTION(BlueprintCallable)
	UStaticMesh* GetMagazineMesh();

	//Flags
	bool BlockFire = false;
	bool bIsIdle = false;
	//Dispersion
	bool ShouldReduceDispersion = true;
	float CurrentDispersion = 0.f;
	float CurrentDispersionMax = 1.f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	FVector ShootEndLocation = FVector(0);
	
	void UpdateStateWeapon(EMovementState MovementState, bool bIsAim);
	void ChangeDispersionByShot();
	UFUNCTION(BlueprintCallable)
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	
	//Debug
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeDirectionLogic = 100.f;
};
