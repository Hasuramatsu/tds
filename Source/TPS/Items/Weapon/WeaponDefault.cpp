// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "TPS/Character/TPSCharacter.h"
#include "TPS/Game/TPSGameInstance.h"



// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);
	
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);
	
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);

	if(WeaponSetting.AnimCharReload)
	{
		ReloadTime = WeaponSetting.AnimCharReload->GetPlayLength();
	}
	else
	{
		ReloadTime = WeaponSetting.ReloadTime;
	}
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if(WeaponFiring && !WeaponReloading)
	{
		if(FireTimer <= 0.f)
		{
			Fire();
		}
	}
	if(FireTimer > -1)
	{
		FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if(WeaponReloading)
	{
		if(ReloadTimer <= 0.f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if(!WeaponFiring)
	{
		if(bIsIdle)
		{
			if(CurrentDispersion > WeaponSetting.DispersionWeapon.DispersionIdleMin || CurrentDispersion > CurrentDispersionMin)
			{
				CurrentDispersion -= CurrentDispersionReduction;
			}
			else
			{
				if(WeaponSetting.DispersionWeapon.DispersionIdleMin < CurrentDispersionMin )
				{
					CurrentDispersion = WeaponSetting.DispersionWeapon.DispersionIdleMin;
				}
				else
				{
					CurrentDispersion = CurrentDispersionMin;
				}
			}	
		}
		else if(ShouldReduceDispersion)
		{
			if(CurrentDispersion > CurrentDispersionMin)
			{
				CurrentDispersion -= CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersionMin;
			}
		}
		else
		{
			if(CurrentDispersion < CurrentDispersionMax)
			{
				CurrentDispersion += CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if(bIsIdle)
	{
		if(CurrentDispersion > WeaponSetting.DispersionWeapon.DispersionIdleMax)
		{
			CurrentDispersion = WeaponSetting.DispersionWeapon.DispersionIdleMax;
		}
	}
	else
	{
		if(CurrentDispersion > CurrentDispersionMax)
		{
			CurrentDispersion = CurrentDispersionMax;
		}
	}
	
	if(ShowDebug)
	{
		UE_LOG(LogTemp,Warning,TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"),
        CurrentDispersionMax,
        CurrentDispersionMin,
        CurrentDispersion);
	}
}


void AWeaponDefault::WeaponInit()
{
	if(SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent();
	}
	
	if(StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if(CheckWeaponCanFire())
	{
			WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
	}
}

void AWeaponDefault::SpawnMagazine()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	FVector Location = SkeletalMeshWeapon->GetSocketLocation("Clip");
	FRotator Rotation = SkeletalMeshWeapon->GetSocketRotation("Clip");
	AStaticMeshActor* Magazine = Cast<AStaticMeshActor>(GetWorld()->SpawnActor(AStaticMeshActor::StaticClass(), &Location, &Rotation,SpawnParams));
	if(Magazine)
	{
		Magazine->SetLifeSpan(1.f);
		UStaticMeshComponent* NewComponent = NewObject<UStaticMeshComponent>(Magazine, UStaticMeshComponent::StaticClass());
		NewComponent->SetSimulatePhysics(true);
		NewComponent->SetCollisionProfileName("PhysicIgnoreAll");
		NewComponent->SetupAttachment(Magazine->GetRootComponent());
		NewComponent->SetStaticMesh(WeaponSetting.MagazineDrop);
		NewComponent->RegisterComponent();
	}
	
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	if(!WeaponReloading && WeaponInfo.Round != 0 && !BlockFire)
	{
		return true;
	}
	else if(WeaponInfo.Round == 0)
	{
		//TODO no ammo sound
	}
	return false;
}

FName AWeaponDefault::GetProjectileID()
{
	return WeaponSetting.ProjectileID;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.ProjectilesByShot;
}

UAnimSequence* AWeaponDefault::GetReloadAnimation()
{
	return WeaponSetting.AnimWeaponReload;
}

UStaticMesh* AWeaponDefault::GetMagazineMesh()
{
	return WeaponSetting.MagazineDrop;
}

void AWeaponDefault::Fire()
{
	if(GetWeaponRound() > 0)
	{
		FireTimer = WeaponSetting.RateOfFire;
		//TODO round logic by weapon class
		WeaponInfo.Round--;
		OnWeaponFireStart.Broadcast(WeaponSetting.AnimCharFire);
		ChangeDispersionByShot();

		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

		int8 NumberProjectile = GetNumberProjectileByShot();
		if(ShootLocation)
		{
			FVector SpawnLocation = ShootLocation->GetComponentLocation();
			FRotator SpawnRotation = ShootLocation->GetComponentRotation();
			FVector EndLocation;
			for(int8 i = 0; i < NumberProjectile; i++)
			{
				EndLocation = GetFireEndLocation();
				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();
				FMatrix myMatrix(Dir, FVector(0,1,0), FVector(0,0,1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				if(bIsProjInit)
				{
					if(ProjectileSettings.Projectile)
					{
						FActorSpawnParameters SpawnParams;
						SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
						SpawnParams.Owner = GetOwner();
						SpawnParams.Instigator = GetInstigator();

						AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileSettings.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
						if(myProjectile)
						{
							myProjectile->InitProjectile(ProjectileSettings);
						}
					}
				}
				else
				{
					FHitResult HitRes;
					FVector End = ((Dir * WeaponSetting.DistanceTrace) + SpawnLocation);
					if(ShowDebug)
					{
						DrawDebugLine(GetWorld(), SpawnLocation, End, FColor::Red, true);
					}
					bool Traced = GetWorld()->LineTraceSingleByChannel(HitRes, SpawnLocation, End, ECC_Visibility);
					if(Traced)
					{
						if(HitRes.bBlockingHit)
						{
							UGameplayStatics::ApplyDamage(HitRes.GetActor(), WeaponSetting.WeaponDamage, GetInstigatorController(), this, NULL);
						}	
					}
				}
				if(SkeletalMeshWeapon->DoesSocketExist("AmmoEject"))
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.SleeveBullets, SkeletalMeshWeapon->GetSocketTransform("AmmoEject"));
				}
			}
		}
	}
	else
	{
		WeaponFiring = false;
		//TODO no ammo sound
		ATPSCharacter* MyCharacter = Cast<ATPSCharacter>(GetOwner());
		if(MyCharacter)
		{
			MyCharacter->TryReloadWeapon();
		}
	}
}

void AWeaponDefault::Fire_BP_Implementation()
{
}

void AWeaponDefault::UpdateStateWeapon(EMovementState MovementState, bool bIsAim)
{
	float coef = 1.f;
	switch (MovementState)
	{
		case EMovementState::Run_State:
			BlockFire = false;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.DispersionMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.DispersionMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.DispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.DispersionReduction;
			break;
		case EMovementState::Crouch_State:
			BlockFire = false;
			coef = WeaponSetting.DispersionWeapon.DispersionCrouchCoef;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.DispersionMax * coef;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.DispersionMin * coef;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.DispersionRecoil * coef;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.DispersionReduction * (1-coef+1);
			break;
		case EMovementState::Sprint_State:
			BlockFire = true;
			SetWeaponStateFire(false);
			break;
	}
	if(bIsAim)
	{
		coef = WeaponSetting.DispersionWeapon.DispersionAimCoef;
		CurrentDispersionMax *= coef;
		CurrentDispersionMin *= coef;
		CurrentDispersionRecoil *= coef;
		CurrentDispersionReduction *= (1-coef+1);
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer = ReloadTime;
	if(WeaponSetting.AnimCharReload)
	{
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload);
		SkeletalMeshWeapon->PlayAnimation(WeaponSetting.AnimWeaponReload, false);
		SpawnMagazine();
	}
}

void AWeaponDefault::InitProjectile(FName ProjectileID)
{
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	if(myGI)
	{
		bIsProjInit = myGI->GetProjectileInfoByName(ProjectileID, ProjectileSettings);
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	if(SkeletalMeshWeapon)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundReloadWeapon, SkeletalMeshWeapon->GetComponentLocation());
	}
	else if(StaticMeshWeapon)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundReloadWeapon, StaticMeshWeapon->GetComponentLocation());
	}
	FireTimer = 0.f;
	OnWeaponReloadEnd.Broadcast(true, WeaponSetting.MaxRound);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if(SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd.Broadcast(false, 0);
	//DropClipFlag = false;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

//TODO remove
float AWeaponDefault::GetReloadTime()
{
    return ReloadTime;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	if(ShowDebug)
	{
		
	}
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0);
	FVector SLocation = ShootLocation->GetComponentLocation();

	FVector tmpV = (SLocation - ShootEndLocation);
	if(tmpV.Size() > SizeVectorToChangeDirectionLogic)
	{
		EndLocation = SLocation + ApplyDispersionToShoot((SLocation - ShootEndLocation).GetSafeNormal()) * -20000.f;
		if(ShowDebug)
		{
			DrawDebugCone(GetWorld(), SLocation,
        -(SLocation - ShootEndLocation),
        WeaponSetting.DistanceTrace,
        GetCurrentDispersion()* PI / 180.f,
        GetCurrentDispersion()* PI / 180.f,
        32, FColor::Emerald, false,
        .1f, (uint8)'\000', 1.0f);
		}
	}
	else
	{
		EndLocation = SLocation + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.f;
		if(ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(),
            ShootLocation->GetForwardVector(),
            WeaponSetting.DistanceTrace,
            GetCurrentDispersion()* PI / 180.f,
            GetCurrentDispersion()* PI / 180.f,
            32, FColor::Emerald, false,
            .1f, (uint8)'\000', 1.0f);
		}	
	}
	if(ShowDebug)
	{
		DrawDebugLine(GetWorld(), SLocation, SLocation + ShootLocation->GetForwardVector() * 500.f, FColor::Cyan, false, 5.f, (uint8)'\000', 1.f);
		DrawDebugLine(GetWorld(), SLocation, ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		DrawDebugLine(GetWorld(),SLocation, EndLocation,FColor::Black, false, 5.f, (uint8)'\000',0.5f);

		DrawDebugSphere(GetWorld(),SLocation + ShootLocation->GetForwardVector()*SizeVectorToChangeDirectionLogic, 10.f,8,FColor::Red, false, 4.f);
	}
	return EndLocation;
}


