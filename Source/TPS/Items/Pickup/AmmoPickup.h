// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/Items/PickUp.h"

#include "AmmoPickup.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API AAmmoPickup : public APickUp
{
	GENERATED_BODY()

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:

	AAmmoPickup();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	EWeaponType WeapontType = EWeaponType::PistolType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	int32 CurrentRound = 0;

	virtual void Interact(AActor* Interactor) override;
};
