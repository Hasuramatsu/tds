// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponPickup.h"


#include "TPS/Character/TPSCharacter.h"
#include "TPS/Game/TPSGameInstance.h"

void AWeaponPickup::BeginPlay()
{
    Super::BeginPlay();
}

AWeaponPickup::AWeaponPickup()
{
    PickupType = EPickupType::WeaponType;    
}

void AWeaponPickup::Interact(AActor* Interactor)
{
    ATPSCharacter* MyCharacter = Cast<ATPSCharacter>(Interactor);
    if(MyCharacter)
    {
        if(Title != MyCharacter->InventoryComponent->WeaponSlots[MyCharacter->CurrentWeaponIndex].ItemName)
        {
            MyCharacter->DropCurrentWeapon();
            MyCharacter->InventoryComponent->PickupWeapon(*this);
        }
    }
}

void AWeaponPickup::InitParams(FName ID, int32 Round)
{
    Title = ID;
    CurrentRound = Round;
    SetDataFromTable();
    if(PickupSkeletalMesh->SkeletalMesh)
    {
        //PickupSkeletalMesh->SetSkeletalMesh(DropInfo.DropSkeletalMesh);
        PickupSkeletalMesh->SetCollisionProfileName("PhysicIgnoreAll");
       // PickupSkeletalMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        PickupSkeletalMesh->SetSimulatePhysics(true);
        CollisionSphere->AttachToComponent(PickupSkeletalMesh, FAttachmentTransformRules::KeepRelativeTransform);
        PickupStaticMesh->DestroyComponent();
    }
    else if(PickupStaticMesh->GetStaticMesh())
    {
       // PickupStaticMesh->SetStaticMesh(DropInfo.DropStaticMesh);
        PickupStaticMesh->SetCollisionProfileName("PhysicIgnoreAll");
       // PickupStaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        PickupStaticMesh->SetSimulatePhysics(true);
        PickupSkeletalMesh->DestroyComponent();
        CollisionSphere->AttachToComponent(PickupStaticMesh, FAttachmentTransformRules::KeepRelativeTransform);
    }
    
}

void AWeaponPickup::SetDataFromTable()
{
    Super::SetDataFromTable();

    if(!Title.IsNone())
    {
        UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
        if(myGI)
        {
            FDropInfo DropInfo;
            if(myGI->GetDropInfoByName(Title, DropInfo))
            {
                WeaponType = DropInfo.WeaponType;
            }
        }
    }
}
