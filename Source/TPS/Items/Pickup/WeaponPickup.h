// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/Items/PickUp.h"
#include "TPS/Items/Weapon/WeaponDefault.h"

#include "WeaponPickup.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API AWeaponPickup : public APickUp
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	AWeaponPickup();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	EWeaponType WeaponType = EWeaponType::PistolType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	int32 CurrentRound = 0;

	virtual void Interact(AActor* Interactor) override;
	void InitParams(FName ID, int32 Round);

	virtual void SetDataFromTable() override;
};
