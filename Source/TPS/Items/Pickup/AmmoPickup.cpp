// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickup.h"

#include "TPS/Character/TPSCharacter.h"



void AAmmoPickup::BeginPlay()
{
    Super::BeginPlay();
}

AAmmoPickup::AAmmoPickup()
{
    PickupType = EPickupType::AmmoType;
    CollisionSphere->AttachToComponent(PickupStaticMesh, FAttachmentTransformRules::KeepRelativeTransform);
}

void AAmmoPickup::Interact(AActor* Interactor)
{
    ATPSCharacter* MyChar = Cast<ATPSCharacter>(Interactor);
    if(MyChar && !MyChar->InventoryComponent->IsAmmoFull(WeapontType))
    {
        MyChar->InventoryComponent->ChangeCurrentAmmo(CurrentRound, WeapontType);
        this->Destroy();
    }
}
