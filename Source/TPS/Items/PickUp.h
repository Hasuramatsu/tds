// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Interface/Interactable.h"

#include "PickUp.generated.h"


UCLASS()
class TPS_API APickUp : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUp();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	USphereComponent* RootSphere = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	USphereComponent* CollisionSphere = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* PickupStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	USkeletalMeshComponent* PickupSkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	UParticleSystemComponent* PickupFX = nullptr;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Component)
	//FDropInfo Info;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Info")
	FName Title;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickup Info")
	EPickupType PickupType = EPickupType::None;

	//TODO add sound component
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor) override;

	UFUNCTION()
	void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
    void CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	virtual void SetDataFromTable();

};



