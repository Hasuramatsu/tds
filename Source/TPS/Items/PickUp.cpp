// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp.h"


#include "Kismet/GameplayStatics.h"
#include "TPS/Character/TPSCharacter.h"
#include "TPS/Game/TPSGameInstance.h"

// Sets default values
APickUp::APickUp()
{
 	//Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	//RootComponent = SceneComponent;
	RootSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Root Sphere"));
	RootSphere->SetSphereRadius(10.f);
	RootSphere->SetCollisionProfileName(TEXT("OverlapOnlyPawn"));
	RootSphere->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	RootSphere->SetSimulatePhysics(true);
	RootComponent = RootSphere;	
	
	PickupSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
    PickupSkeletalMesh->SetGenerateOverlapEvents(false);
    PickupSkeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
	PickupSkeletalMesh->SetupAttachment(RootComponent);
	
	PickupStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	PickupStaticMesh->SetGenerateOverlapEvents(false);
	PickupStaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	PickupStaticMesh->SetupAttachment(RootComponent);

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	CollisionSphere->SetSphereRadius(40.f);
	CollisionSphere->SetCanEverAffectNavigation(false);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionSphere->SetupAttachment(RootComponent);

	PickupFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FX"));
	PickupFX->SetupAttachment(RootComponent);
	PickupFX->SetWorldLocation(GetActorLocation());

}

// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();

	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &APickUp::CollisionSphereBeginOverlap);
	CollisionSphere->OnComponentEndOverlap.AddDynamic(this,&APickUp::CollisionSphereEndOverlap);

	SetDataFromTable();
	
	if(PickupSkeletalMesh->SkeletalMesh)
	{
		PickupSkeletalMesh->SetCollisionProfileName("PhysicIgnoreAll");
		//PickupSkeletalMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PickupSkeletalMesh->SetSimulatePhysics(true);
		//CollisionSphere->AttachToComponent(PickupSkeletalMesh, FAttachmentTransformRules::KeepRelativeTransform);
		PickupStaticMesh->DestroyComponent();
	}
	else if(PickupStaticMesh->GetStaticMesh())
	{
		PickupStaticMesh->SetCollisionProfileName("PhysicIgnoreAll");
		//PickupStaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PickupStaticMesh->SetSimulatePhysics(false);
		PickupSkeletalMesh->DestroyComponent();
		//CollisionSphere->AttachToComponent(PickupStaticMesh, FAttachmentTransformRules::KeepRelativeTransform);
	}
}

// Called every frame
void APickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUp::Interact(AActor* Interactor)
{
	
}


void APickUp::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ATPSCharacter* myChar = Cast<ATPSCharacter>(OtherActor);
	if(myChar)
	{
		myChar->OverlapPickup(this);	
	}
}

void APickUp::CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ATPSCharacter* myChar = Cast<ATPSCharacter>(OtherActor);
	if(myChar)
	{
		myChar->OverlapPickupEnd(this);	
	}
}

void APickUp::SetDataFromTable()
{
	if(!Title.IsNone())
	{
		UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
		if(myGI)
		{
			FDropInfo DropInfo;
			if(myGI->GetDropInfoByName(Title, DropInfo))
			{
				if(!PickupSkeletalMesh->SkeletalMesh && DropInfo.DropSkeletalMesh)
				{
					PickupSkeletalMesh->SetSkeletalMesh(DropInfo.DropSkeletalMesh);
				}
				else if(!PickupStaticMesh->GetStaticMesh() && DropInfo.DropStaticMesh)
				{
					PickupStaticMesh->SetStaticMesh(DropInfo.DropStaticMesh);
				}
				if(!PickupFX->Template && DropInfo.DropFX)
				{
					PickupFX->Template = DropInfo.DropFX;
					PickupFX->Activate();
				}
				// if(!PickupSound && DropInfo.DropSound)
				// {
				// 	PickupSound = DropInfo.DropSound;
				// }
			}
		}
	}
}
